# Лекция 11

> 14.05.19

Особенности слоя batch-normalization:
1. Число нейронов равно числу нейронов слоя, к которому эта штука применяется.
2. Матрица синаптических коэффициентов диагональная.
3. Активационные характеристики линейные.
4. В ходе обучения подстраиваются не синаптические коэффициенты, а связанные с ними $`\gamma`$ и $`\beta`$. Чтобы вычислить син.коэффициенты и смещение, нужно знать $`\bar{y}`$ и $`S`$. Нужно сначала прогонять выборку до слоя batch-normalization, и только после вычисления требуемых величин начинать прогонять дальше.
5. Градиенты $`\frac{\partial E^p}{\partial \gamma_j^l}`$ и $`\frac{\partial E^p}{\partial \beta_j^l}`$ зависят от остальных примеров mini-batch.

**Обратное распространение на batch-normalization слое.**

$`y`$ - выходы самого слоя  
$`v`$ - синаптические коэффициенты batch-normalization слоя  
$`-b`$ - смещения batch-normalization слоя  
$`N_l`$ - количество нейронов в рассматриваемом слое  

```math
\frac {
    \partial E
}{
    \partial \gamma_j^l
}
=
\frac {
    \partial E
}{
    \partial \tilde{h}_j^l
}
\frac {
    \partial \tilde{h}_j^l
}{
    \partial \gamma_j^l
}
=
\left(
\sum_{m=1}^{N_{l+1}}
\frac {
    \partial E
}{
    \partial h_m^{l+1}    
}
\frac {
    \partial h_m^{l+1}
}{
    \partial \tilde{h}_j^l
}
\right)
\frac {
    \partial h_j^l
}{
    \partial \gamma_j^l
}
=
\underbrace{
\left(
    \sum_{m=1}^{N_{l+1}}
        \Delta_m^{l+1}w_{mj}^{l+1} 
\right)
}_{\tilde{\Delta}_j^l}
\frac {
    y_j^l-\bar{y}_j^l
}{
    S_j^l
}
```
```math
\frac {
    \partial E
}{
    \partial \beta_j^l
}
=
\frac {
    \partial E
}{
    \partial \tilde{h}_j^l
}
\frac {
    \partial \tilde{h}_j^l
}{
    \partial \beta_j^l
}
=
\tilde{\Delta}_j^l
```
```math
\Delta_j^l
=
\frac {
    \partial E
}{
    \partial \tilde{h}_j^l
}
\frac {
    \partial \tilde{h}_j^l
}{
    \partial h_j^l
}
=
\tilde{\Delta}_j^l v_j^l f'_l(h_j^l)
```

В финальной сети слой batch-normalization остаётся. Единственное, в качестве $`\bar{y}`$ и $`S`$ выбираются значения по всей обучающей выборке.