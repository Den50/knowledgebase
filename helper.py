import sys
import os

additional = """---
id: cheat
---
@import "../../style.less"
"""

diraliases = {
    "aiintro": "Введение в ИнСист",
    "tns": "Введение в теорию нейронных сетей",
    "dm4": "ДМ-4. Логические исчисления",
    "dm5": "ДМ-5. Математическая лингвистика",
    "ids": "Интеллектуальные диалоговые системы",
    "matstat": "Математическая статистика",
    "mo": "Методы оптимизации"
}


def sortByValue(item):
    return item[1]


args = sys.argv[1:]

if len(args) < 1:
    print("Wrong number of arguments: there should be 1 at least: <command>\nTry tap `help` to get help.")
    exit()

cmd = args[0]

if cmd == "discipline":
    if len(args) != 2:
        print("Wrong number of arguments: there should be two: <command> <directory>.")
        exit()
    directory = args[1]
    if not os.path.exists(directory):
        os.makedirs(directory)
        os.makedirs(directory + "/images")
        os.makedirs(directory + "/questions")
        os.makedirs(directory + "/lections")
        os.makedirs(directory + "/seminars")
elif cmd == "cheat":
    if len(args) != 2:
        print("Wrong number of arguments: there should be two: <command> <directory>.")
        exit()
    if (args[1] not in diraliases):
        print("Unknown alias for discipline. Use `python hepler.py ls` to list all possibilities.")
        exit()
    directory = diraliases[args[1]]
    if os.path.exists(directory):
        questions = os.listdir(directory + "/questions")
        for question in questions:
            content = ""
            with open("./"+directory+"/questions/"+question, "r", encoding='utf-8') as file:
                content = file.read()
            with open("./"+directory+"/questions/"+question, "w", encoding='utf-8') as file:
                file.write(additional)
                file.write(content)
elif cmd == "uncheat":
    if len(args) != 2:
        print("Wrong number of arguments: there should be two: <command> <directory>.")
        exit()
    if (args[1] not in diraliases):
        print("Unknown alias for discipline. Use `python hepler.py ls` to list all possibilities.")
        exit()
    directory = diraliases[args[1]]
    if os.path.exists(directory):
        questions = os.listdir(directory + "/questions")
        for question in questions:
            content = ""
            with open("./"+directory+"/questions/"+question, "r", encoding='utf-8') as file:
                content = file.read()
            if content.find(additional) != -1:
                content = content.replace(additional, "")
            with open("./"+directory+"/questions/"+question, "w", encoding='utf-8') as file:
                file.write(content)
elif cmd == "ls":
    maxlen = max(len(x) for x in diraliases.values())
    for alias, directory in sorted(diraliases.items(), key=sortByValue):
        print("|{value}{spaces} -> {key}".
              format(key=alias, value=directory,
                     spaces=" "*(maxlen-len(directory)))
              )
elif cmd == "help":
    print("""Usage:
* discipline <discipline-name> - create folder with structured subfolders
* cheat <discipline-name> - use `style.less` for all questions to make them better on printing
* uncheat <discipline-name> - removes activity of `cheat` function
* ls - lists all aliases for disciplines in alphabetical order""")
