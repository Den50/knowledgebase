# Knowledge base

## Использование
1. Установить VSCode.
2. Установить расширение *Markdown Preview Enhanced* от *Yiyi Wang*.
3. Залезть в *Settings* (*Ctrl+,*) -> *Extensions* -> *Markdown Preview Enhanced* и установить:
    - Math Rendering Option: `katex`
    - Math Inline Delimiters: добавить в файл следующую строку: 
    ```code
    "markdown-preview-enhanced.mathInlineDelimiters": [["$`","`$"]]
    ```
    - Mermaid Theme: менять настройку, пока не начнут отображаться дуги на графах.  
4. Перезагрузить VSCode.
5. Установить расширение *Code Spell Checker*. А также дополнение к нему - *Russian - Code Spell Checker*.
6. Активировать проверку русской орфографии теперь можно так: *Ctrl+Shift+P* $`\to`$ *Enable Russian Spell Cheker Dictionary*.
7. Введите в консоль команду `git config --global core.quotepath false` для корректного отображения русского текста в консоли.
8. 

## Преобразование markdown в pdf
1. Открыть превью .md файла. Возможные варианты:
    - Правой кнопкой на текст .md файла, затем *Markdown Preview Enhanced: Open Preview*.
    - Сверху справа есть иконки. Одна из них (с серой лупой) нужное.
    - Комбинация *Ctrl+K V*.
2. Тыкнуть правой кнопкой по тексту превью. Вылезет окошко с возможными вариантами конвертирования и другими опциями. 

## Руководства
* Справка по [markdown](https://paulradzkov.com/2014/markdown_cheatsheet/).
* Справка по [katex](https://katex.org/docs/supported.html).
* Огромный список символов [latex unicode](http://milde.users.sourceforge.net/LUCR/Math/).
* Справка по [mermaid](https://mermaidjs.github.io/flowchart.html).

## Дополнительные вещи

Gitlab отображает markdown в стиле `gitlab`, рендерит математику с помощью $`\KaTeX`$, поэтому есть ряд отличий:
1. Строчные формулы вставляются не с помощью знаков <code>$$</code>, а вот такой конструкцией: 
    ```code
    $`y=x^2`$
    ```
2. Большие формулы оформляются, как будто это код:
    ```code
    ```math
    F = \frac
        {\alpha X}
        {\beta Y + \gamma Z}
    ```.
    ```

## Возможности

В корне репозитория лежит файл `helper.py`, который призван помочь автоматизировать работу с markdown-файлами. Текущая функциональность:
* `$ python helper.py discipline <название дисциплины>` - создаёт папку с именем <название дисциплины> и некоторые внутренние папки
* `$ python helper.py cheat <алиас дисциплины>` - добавляет в заголовок всех файлов, лежащих в подкаталоге `questions` соответствующей дисциплины импорт файла стилей `style.less`, подготавливающего markdown-файл для удобной печати шпор
* `$ python helper.py uncheat <алиас дисциплины>` - возвращает всё как было
* `$ python helper.py ls` - показывает список алиасов всех дисциплин