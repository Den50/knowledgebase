## 20. Нотации, применяемые для задания КС-грамматик.

БНФ - Бекусова нормальная форма.

```math
\text{БНФ}=\begin{cases}
\text{1. Нетерминалы заключаются в <>} \\
\text{2. Знак }\rightarrow\space\text{записывается ::=}  \\
\text{3. Альтернативы отделяются |}  \\
\text{4. }\bold{\large\lambda}\text{ не обозначаются}
\end{cases}\text{  }
```

РБНФ - расширенная Бекусова нормальная форма.

```math
\text{РБНФ}=\begin{cases}
\text{1. Нетерминалы обозначаюся словами} \\
\text{2. Терминалы заключаются в апострофы ('a')}  \\
\text{3. Альтернативы отделяются |}  \\
\text{4. }\rightarrow\text{ заменяется на =}  \\
\text{5. Необязательная конструкция в [ ]}  \\
\text{6. 0 и более раз в \{ \}}  \\
\text{7. ( ) служат для факторизации, т.е. }\varphi\psi|\gamma\psi\Leftrightarrow(\varphi|\gamma)\psi  \\
\text{8. В конце каждого описания ставится точка}
\end{cases}
```

Синтаксическая диаграмма - графическая форма представления РБНФ

1. Нетерминалы заключаются в прямоугольники.
2. Терминалы заключаются в овалы.
3. Образы нетерминалов и терминалов соединяются линиями таким образом, чтобы множество путей соответствовало множеству цепочек из терминалов и нетерминалов, которое задаётся РБНФ.
![](../images/q20.jpg)